/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc1799.WiredUp1;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    
    public static final int kGyroChannel = 0;
    
    public static class eInvertMotor {
	public static final boolean eRightmotor = false;
    public static final boolean eLeftmotor = false;
    }
    
    public static class ePWM {
        public static final int pwm0 = 0;
        public static final int pwm1 = 1;
        public static final int pwm2 = 2;
        public static final int pwm3 = 3;
        public static final int pwm4 = 4;
        public static final int pwm5 = 5;
        public static final int pwm6 = 6;
        public static final int pwm7 = 7;
        public static final int pwm8 = 8;
        public static final int pwm9 = 9;
    }

    public static class eWheelDrive {
        public static final int leftChannel = ePWM.pwm0;
        public static final int rightChannel = ePWM.pwm1;
    }

    public static class eCamDrive {
        public static final int botCamChanne = ePWM.pwm2;
        public static final int topCamChanne = ePWM.pwm3;
    }


    public static class eJoystick {
        public static final int ChannelOneRC = 0;
        public static final int ChannelTwoRC = 1;
    }
    //XBOX Joystick ports
    public static class eJoyStickKeyMap {
        public static final int kTurboLeft = 9;
        public static final int kTurboRight =  10;
        public static final int kStickLeft  =  1;
        public static final int webXtickLeft = 0;
        public static final int webXStickRight = 4;
        public static final int webYtickLeft = 1;
        public static final int webYStickRight = 5;
        public static final int kStickRight  = 5 ;
        public static final int kA  =  1;
        public static final int kClawOpenKey  =  1;
        public static final int kClawCloseKey  =  4;
        
        // public static final int kB  =  2;
        // public static final int kX  =  3;
        // public static final int kY  =  4;
        // public static final int kBack  =  7;
        // public static final int kStart  =  8;
    }



    public static class eClaw {
        public static final int ClawChannel = ePWM.pwm3;
        public static final Boolean cinverted = false;
        public static final double JSClawSpeed = 0.25;
        public static final double AutodriveClawSpeed = 0.25;

    }

    public static class eArm {
        public static final int ArmChannel = ePWM.pwm2;
    }

    public static class eWebCamServo {
        public static final int TopDriveChannel = ePWM.pwm9;
        public static final int BaseDriveChannel = ePWM.pwm8;
    }

    public static class CAN {
		public static final int pcmId = 0;
	}
}
