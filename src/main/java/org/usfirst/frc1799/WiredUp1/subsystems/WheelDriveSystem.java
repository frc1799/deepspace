/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc1799.WiredUp1.subsystems;

import org.usfirst.frc1799.WiredUp1.RobotMap.*;
import org.usfirst.frc1799.WiredUp1.commands.DriveByJoyStick;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

/**
 * Add your docs here.
 */
public class WheelDriveSystem extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private VictorSP m_leftController;
  private VictorSP m_rightController;
  private DifferentialDrive m_differentialDrive;

  public WheelDriveSystem() {
		m_leftController = new VictorSP(eWheelDrive.leftChannel);
		m_rightController = new VictorSP(eWheelDrive.rightChannel);

		// Invert the left side motors.
		// You may need to change or remove this to match your robot.
		m_leftController.setInverted(eInvertMotor.eLeftmotor);
    m_rightController.setInverted(eInvertMotor.eRightmotor);
    
    addChild("leftController",m_leftController);
    addChild("rightController",m_rightController);

    m_differentialDrive = new DifferentialDrive(m_leftController, m_rightController);
    addChild("DifferentialDrive",m_differentialDrive);
    m_differentialDrive.setSafetyEnabled(true);
    m_differentialDrive.setExpiration(0.2);
    m_differentialDrive.setMaxOutput(1.0);
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new DriveByJoyStick());
  }

  /**
   * Stop the drivetrain from moving.
   */
  public void stop() {
    m_differentialDrive.tankDrive(0, 0);
  }
 /**
   * go: run the drivetrain by axis.
   */
  public void go(double leftSpeed, double rightSpeed) {

    if (leftSpeed < -1 || leftSpeed > 1) {
      leftSpeed = 0;
    }
    if (rightSpeed < -1 || rightSpeed > 1) {
      rightSpeed = 0;
    }
    m_differentialDrive.tankDrive(leftSpeed, rightSpeed, true);
  }
 /**
   * go: run the drivetrain by joystick.
   */
  public void go(XboxController JoyStick) {
    this.go(JoyStick.getRawAxis(eJoyStickKeyMap.kStickLeft), JoyStick.getRawAxis(eJoyStickKeyMap.kStickRight));
  }

}
