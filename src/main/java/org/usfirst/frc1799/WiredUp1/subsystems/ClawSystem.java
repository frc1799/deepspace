/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc1799.WiredUp1.subsystems;

import org.usfirst.frc1799.WiredUp1.RobotMap.eClaw;
import org.usfirst.frc1799.WiredUp1.RobotMap.eJoyStickKeyMap;
import org.usfirst.frc1799.WiredUp1.commands.ClawStopCmd;

import java.lang.Math;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Add your docs here.
 */
public class ClawSystem extends Subsystem {
  private VictorSP m_Claw;
  private double setAutoDriveSpeed = 0.25;
  private double setJoyStickDriveSpeed = 0.25;

  
  public ClawSystem() {
  VictorSP m_Claw = new VictorSP(eClaw.ClawChannel);
      m_Claw.setInverted(eClaw.cinverted);
        
      addChild("claw",m_Claw);

  }
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new ClawStopCmd());
  }

 /**
   * go: set claw Auto Drive speed
   */
  public void setAutoDriveSpeed(double cseed) {
    setAutoDriveSpeed = Math.abs(cseed);
  }

   /**
   * go: set claw Joy Stick Drivespeed
   */
  public void setJoyStickDriveSpeed(double cseed) {
    setJoyStickDriveSpeed = Math.abs(cseed);
  }

 /**
   * go: drive to close
   */
  private void drivetoclose(double setSpeed) {
    m_Claw.setSpeed(Math.abs(setSpeed));
  }

  
 /**
   * go: drive to close
   */
  public void drivetoclose() {
    this.drivetoclose(Math.abs(setAutoDriveSpeed));
  }
  
 /**
   * go: drive to open
   */
  private void drivetoopen(double setSpeed) {
    m_Claw.setSpeed(-1 * Math.abs(setSpeed)); //-ve for opposite direction
  }

  /**
   * go: drive to open
   */
  public void drivetoopen() {
    this.drivetoopen(-1 * Math.abs(setAutoDriveSpeed)); //-ve for opposite direction
  }
  
 /**
   * go: stop.
   */
  public void stop() {
    m_Claw.setSpeed(0);
  }

   /**
   * go: Claw drive  by joystick.
   */
  public void driveClawByJoystick(XboxController JoyStick) {
    if(JoyStick.getPOV() == 0){
      System.out.println("POV 0");
      this.drivetoclose(setJoyStickDriveSpeed); //
    }else if (JoyStick.getPOV() == 90){
      System.out.println("POV 90");
      this.drivetoopen(setJoyStickDriveSpeed); //
    }else{
      System.out.println("POV");
      System.out.println(JoyStick.getPOV());
    }
  }
  

}
