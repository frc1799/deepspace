/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.usfirst.frc1799.WiredUp1.commands;

import org.usfirst.frc1799.WiredUp1.Robot;
import org.usfirst.frc1799.WiredUp1.RobotMap.eClaw;

import edu.wpi.first.wpilibj.command.Command;

public class ClawDriveByJoyStick extends Command {
  public ClawDriveByJoyStick() {
    // Use requires() here to declare subsystem dependencies
   requires(Robot.pClawSystem);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.pClawSystem.setJoyStickDriveSpeed(eClaw.JSClawSpeed);//0.25
    Robot.pClawSystem.setAutoDriveSpeed(eClaw.AutodriveClawSpeed);//0.25
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.pClawSystem.driveClawByJoystick(Robot.m_oi.getDriveJoystick());
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    Robot.pClawSystem.stop();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
    Robot.pClawSystem.stop();
  }
}
