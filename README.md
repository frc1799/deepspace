# DeepSpace

FRC  year 2019

This repo will host code we will work on for FRC DeepSpace year 2019

## Pending work as of March 15
Arm Up / down:  with/with out encoder
Arm calibrate with limit switch: 
Claw open/close:  no encoder
Claw calibrate with limit: 
Assigning keys to all the position: 
Autonomous FF drive: 
Lights:
Gyro:
Network radio programming Camera with network driver 			


## Teaser Video 
http://info.firstinspires.org/2019-frc-teaser

## Game & Season Materials page 
https://www.firstinspires.org/resource-library/frc/competition-manual-qa-system
Encryption code - $Robots&in#SPACE!!

https://firstfrc.blob.core.windows.net/frc2019/Manual/2019FRCGameSeasonManual.pdf

https://firstfrc.blob.core.windows.net/frc2019/EventRules/EventRulesManual.pdf

https://firstfrc.blob.core.windows.net/frc2019/Manual/TeamUpdates/TeamUpdates-combined.pdf


## soffware how to
https://gitlab.com/frc1799/2019frc_readme_docs/tree/Mano_branch



### watch below videos on 2019 how to
*   https://www.youtube.com/watch?v=B1DHUEUzqps
*   https://www.youtube.com/watch?v=ych21L5NhdQ
*   https://www.youtube.com/watch?v=aeY8_WnalGw


### git and gitlab how to
*   todo
*   git and gitlab : basics https://www.youtube.com/watch?v=BCQHnlnPusY&list=PLRqwX-V7Uu6ZF9C0YMKuns9sLDzK6zoiV

